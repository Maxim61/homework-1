//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include <string.h>


const char* ZERO = "zero";
const char* ONE = "one";
const char* TWO = "two";
const char* THREE = "three";
const char* FOUR = "four";
const char* FIVE = "five";
const char* SIX = "six";
const char* SEVEN = "seven";
const char* EIGHT = "eight";
const char* NINE = "nine";
const char* DOT = "dot";
const char* COMMA = "comma";
const char* ERROR = "ERROR";

const int SIZE_ZERO = 4;
const int SIZE_ONE = 3;
const int SIZE_TWO = 3;
const int SIZE_THREE = 5;
const int SIZE_FOUR = 4;
const int SIZE_FIVE = 4;
const int SIZE_SIX = 3;
const int SIZE_SEVEN = 5;
const int SIZE_EIGHT = 5;
const int SIZE_NINE = 4;
const int SIZE_DOT = 3;
const int SIZE_COMMA = 5;
const int SIZE_ERROR = 5;

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    //srand(time(NULL));
    long ** arrPointer = (long**) calloc(rows,sizeof(long));
    for(int i = 0; i < rows; ++i)
    {
       arrPointer[i] = (long*) calloc(columns, sizeof(long));
       for(int j = 0; j < columns; ++j)
       {
            arrPointer[i][j] = 	rand() % 200 - 100;
        }
    }
    
    return arrPointer;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    unsigned long strSize = strlen(numberString);
    int numbCounter = 1;
    while (numberString[strSize] != '\0')
    {
        if (numberString[strSize] == ' ')
        {
            ++numbCounter;
        }
    }
    
    char** arrayStringsNumb = (char**) calloc(numbCounter, sizeof(char*));
    int currentWordSize = 0;
    int currentWord = 0;
    int finalstrSize = 0;
    
    for (int i = 0; i <= strSize; ++i)
    {
        if ((numberString[i] == ' ') || (numberString[i] == '\0'))
        {
            int counter = 0;
            arrayStringsNumb[currentWord] = (char*) calloc(currentWordSize, sizeof(char));
            for(int j = i - currentWordSize; j < i; ++j)
            {
                switch (numberString[j]) {              
                    case '0':
                        finalstrSize += SIZE_ZERO;
                        break;
                    case '1':
                        finalstrSize += SIZE_ONE;
                        break;
                    case '2':
                        finalstrSize += SIZE_TWO;
                        break;
                    case '3':
                        finalstrSize += SIZE_THREE;
                        break;
                    case '4':
                        finalstrSize += SIZE_FOUR;
                        break;
                    case '5':
                        finalstrSize += SIZE_FIVE;
                        break;
                    case '6':
                        finalstrSize += SIZE_SIX;
                        break;
                    case '7':
                        finalstrSize += SIZE_SEVEN;
                        break;
                    case '8':
                        finalstrSize += SIZE_EIGHT;
                        break;
                    case '9':
                        finalstrSize += SIZE_NINE;
                        break;
                    case '.':
                        finalstrSize += SIZE_DOT;
                        break;
                    case ',':
                        finalstrSize += SIZE_COMMA;
                        break;
                    case 'a'-'Z':
                        finalstrSize += SIZE_ERROR;
                        break;
                    default:
                        break;
                }
                ++finalstrSize;
                arrayStringsNumb[currentWord][counter] = numberString[j];
                ++counter;
            }
            
            ++currentWord;
            currentWordSize = 0;
        }
        
        ++currentWordSize; 
    }
    
    --finalstrSize;
    currentWord = 0;
    
    char* resultWordStirng = (char*) calloc(finalstrSize, sizeof(char));
    int currentPos = 0;
    for(int i = 0; i < numbCounter; ++i)
    {
        int counter = 0;
        while (arrayStringsNumb[currentWord][counter] != '\0')
        {
            switch (arrayStringsNumb[currentWord][counter]) {              
                case '0':
                    for(int k = currentPos; k < currentPos + SIZE_ZERO; ++k)
                    {
                        resultWordStirng[k] = ZERO[k - currentPos];
                    }
                    currentPos += SIZE_ZERO;
                    break;
                case '1':
                    for(int k = currentPos; k < currentPos + SIZE_ONE; ++k)
                    {
                        resultWordStirng[k] = ONE[k - currentPos];
                    }
                    currentPos += SIZE_ONE;
                    break;
                case '2':
                    for(int k = currentPos; k < currentPos + SIZE_TWO; ++k)
                    {
                        resultWordStirng[k] = TWO[k - currentPos];
                    }
                    currentPos += SIZE_TWO;
                    break;
                case '3':
                    for(int k = currentPos; k < currentPos + SIZE_THREE; ++k)
                    {
                        resultWordStirng[k] = THREE[k - currentPos];
                    }
                    currentPos += SIZE_THREE;
                    break;
                case '4':
                    for(int k = currentPos; k < currentPos + SIZE_FOUR; ++k)
                    {
                        resultWordStirng[k] = FOUR[k - currentPos];
                    }
                    currentPos += SIZE_FOUR;
                    break;
                case '5':
                    for(int k = currentPos; k < currentPos + SIZE_FIVE; ++k)
                    {
                        resultWordStirng[k] = FIVE[k - currentPos];
                    }
                    currentPos += SIZE_FIVE;
                    break;
                case '6':
                    for(int k = currentPos; k < currentPos + SIZE_SIX; ++k)
                    {
                        resultWordStirng[k] = SIX[k - currentPos];
                    }
                    currentPos += SIZE_SIX;
                    break;
                case '7':
                    for(int k = currentPos; k < currentPos + SIZE_SEVEN; ++k)
                    {
                        resultWordStirng[k] = SEVEN[k - currentPos];
                    }
                    currentPos += SIZE_SEVEN;
                    break;
                case '8':
                    for(int k = currentPos; k < currentPos + SIZE_EIGHT; ++k)
                    {
                        resultWordStirng[k] = EIGHT[k - currentPos];
                    }
                    currentPos += SIZE_EIGHT;
                    break;
                case '9':
                    for(int k = currentPos; k < currentPos + SIZE_NINE; ++k)
                    {
                        resultWordStirng[k] = NINE[k - currentPos];
                    }
                    currentPos += SIZE_NINE;
                    break;
                case '.':
                    for(int k = currentPos; k < currentPos + SIZE_DOT; ++k)
                    {
                        resultWordStirng[k] = DOT[k - currentPos];
                    }
                    currentPos += SIZE_DOT;
                    break;
                case ',':
                    for(int k = currentPos; k < currentPos + SIZE_COMMA; ++k)
                    {
                        resultWordStirng[k] = COMMA[k - currentPos];
                    }
                    currentPos += SIZE_COMMA;
                    break;
                case 'a'-'Z':
                    for(int k = currentPos; k < currentPos + SIZE_COMMA; ++k)
                    {
                        resultWordStirng[k] = ERROR[k - currentPos];
                    }
                    currentPos += SIZE_ERROR;
                    break;
                default:
                    break;
            }
            
            if (arrayStringsNumb[currentWord][counter + 1] != '\0')
            {
                resultWordStirng[currentPos] = ' ';
                ++currentPos;
            }
            ++counter;
        }
        
        ++currentWord;
        if (currentWord != numbCounter) 
        {
            resultWordStirng[currentPos] = ',';
            resultWordStirng[currentPos + 1] = ' ';
            currentPos += 2;
        }
        
    }    
    return resultWordStirng;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
     
    double x, y;
    
    x = fmod(time * 16 , canvasSize);
    y = (sin(x / 16) * (canvasSize / 2)) + canvasSize / 2;
    
    return (HWPoint){x, y};
}

