//
//  HWAnimationView.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 28/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWAnimationView.h"
#import "HWAnimationConstants.h"

NSInteger const HWAnimationViewDefaultPointColorHex = HWLeafColorHex;

@interface HWAnimationView ()

@property (nonatomic, strong) NSMutableArray<NSValue *> *points;
@property (nonatomic, assign) CGColorRef pointCGColor;

@end

@implementation HWAnimationView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.layer.cornerRadius = 10.0f;
        self.clipsToBounds = YES;
        
        _pointsThreshold = HWDefaultPointsThreshold;
        _pointSize = HWDefaultPointSize;
        _pointColor = [UIColor hw_colorWithHexInt:HWLeafColorHex];
        _pointCGColor = _pointColor.CGColor;
        _points = [[NSMutableArray alloc] initWithCapacity:_pointsThreshold];
    }
    return self;
}

#pragma mark - Accessors

- (void)setPointSize:(CGFloat)pointSize {
    _pointSize = pointSize;
    [self setNeedsDisplay];
}

- (void)setPointColor:(UIColor *)pointColor {
    _pointColor = pointColor;
    _pointCGColor = _pointColor.CGColor;
    [self setNeedsDisplay];
}

#pragma mark - Public methods

- (void)addPoint:(CGPoint)point {
    [self.points insertObject:[NSValue valueWithCGPoint:point] atIndex:0];
    if (self.points.count > self.pointsThreshold) {
        [self.points removeLastObject];
    }
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.points enumerateObjectsUsingBlock:^(NSValue  *value, NSUInteger idx, BOOL * _Nonnull stop) {
        CGColorRef color = CGColorCreateCopyWithAlpha(self.pointCGColor, (CGFloat)(self.pointsThreshold - idx) / (CGFloat)self.pointsThreshold);
        
        CGContextSetFillColorWithColor(context, color);
        CGContextFillEllipseInRect (context, CGRectMake(value.CGPointValue.x - self.pointSize / 2.0f,
                                                        value.CGPointValue.y - self.pointSize / 2.0f,
                                                        self.pointSize,
                                                        self.pointSize));
        CGColorRelease(color);
    }];
}

#pragma mark - Override 

- (CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake(size.width, size.width);
}

@end
